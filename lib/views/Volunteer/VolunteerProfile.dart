import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:foodbees/globals.dart';
import 'package:foodbees/services/FB.dart';
import 'package:foodbees/widget/ProfileCover.dart';
import 'package:foodbees/widget/rounded_single_info_tile.dart';


class VolunteerProfile extends StatefulWidget {
  FB fb = new FB();
  @override
  _VolunteerProfileState createState() => _VolunteerProfileState();
}

class _VolunteerProfileState extends State<VolunteerProfile> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      /*appBar: AppBar(
        title: Text('DonorHome'),
      ),*/
      body: SingleChildScrollView(

        child: Column(

          children: <Widget>[
            ProfileCover(
              name: "Ronaldo",
              color: Color.fromARGB(255, 229, 41, 62),
              logoutIconColor: Colors.white,
              link:
              "https://www.fcbarcelonanoticias.com/uploads/s1/11/89/45/0/cristiano-ronaldo-warm.jpeg",
            ),
            SizedBox(height: 60),
            Container(
              padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
              child: Column(

                children: <Widget>[
                  RoundedSingleInfoTile(
                    title: loginUser.name ,
                    iconData: Icons.account_box,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  RoundedSingleInfoTile(
                    title: loginUser.email,
                    iconData: Icons.email,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  RoundedSingleInfoTile(
                    title: loginUser.mobile,
                    iconData: Icons.call,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  RoundedSingleInfoTile(
                    title: loginUser.userType,
                    iconData: Icons.accessibility,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Container(
                    width: double.infinity,


                    child: RaisedButton(
                        padding: EdgeInsets.symmetric(vertical: 15),
                        color: Color.fromARGB(255, 229, 41, 62),

                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30)
                        ),
                        child: Text(
                          "Edit Profile",
                          style: TextStyle(fontSize: 18,
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        onPressed: () {}
                    ),
                  ),

                  SizedBox(height: 10,),
                  Container(
                    width: double.infinity,


                    child: RaisedButton(
                        padding: EdgeInsets.symmetric(vertical: 15),
                        color: Colors.black87,

                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30)
                        ),
                        child: Text(
                          "Sign Out",
                          style: TextStyle(fontSize: 18,
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        onPressed: () {
                          widget.fb.auth.signOut().then((v) {
                            setState(() {
                              Navigator.pushNamed(context, '/');
                              loginUser = null;
                            });
                          });
                        }
                    ),
                  ),

                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
