import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:foodbees/globals.dart';
import 'package:foodbees/models/Donation.dart';
import 'package:foodbees/models/User.dart';
import 'package:foodbees/services/FB.dart';
import 'package:foodbees/widget/rounded_info_tile.dart';
import 'package:intl/intl.dart';

class DonationDetailsVolunteer extends StatefulWidget {
  FB fb = FB();

  @override
  _DonationDetailsVolunteerState createState() =>
      _DonationDetailsVolunteerState();
}

class _DonationDetailsVolunteerState extends State<DonationDetailsVolunteer> {
  Donation donation = null;

  updateDonation(Donation donation, donationStatus ds, BuildContext bc) {
    widget.fb.store
        .collection(Collections.donations)
        .document(donation.id)
        .updateData({'status': ds.index,'acceptedTime':DateTime.now(),'volunteer':loginUser.id,'partialStatus':1}).then((r) {
      Navigator.pop(bc);
    });
  }

  @override
  Widget build(BuildContext context) {
    if (ModalRoute.of(context).settings.arguments != null) {
      donation = (ModalRoute.of(context).settings.arguments as Map)['donation'];
    }

    return Scaffold(
      backgroundColor: Color.fromARGB(255, 229, 41, 62),
      body: Center(
        child: ListView(
          padding: EdgeInsets.all(20),
          children: <Widget>[
            StreamBuilder(
              stream: widget.fb.store
                  .collection(Collections.users)
                  .where('id', isEqualTo: donation.donorID)
                  .snapshots(),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  User donor = User.fromJson(
                      (snapshot.data.documents[0] as DocumentSnapshot).data);
                  return Column(
                    children: <Widget>[
                      SizedBox(
                        height: 30,
                      ),
                      SizedBox(
                        child: Text(
                          "Donation Details",
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 25,
                            color: Colors.black87,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      RoundedInfoTile(
                        title: "Donator",
                        value: donor.name,
                      ),
                      RoundedInfoTile(
                        title: "Institution",
                        value: donor.institution,
                      ),
                      RoundedInfoTile(title: "Food", value: donation.food),
                      RoundedInfoTile(title: "Amount", value: donation.amount),
                      RoundedInfoTile(
                          title: "Location", value: donation.location),
                      DoubleRoundedInfoTile(
                          title1: "Pick up time",
                          value1: DateFormat("dd MMM, yy")
                              .format(donation.pickUpDate),
                          value2: donation.pickUpTime),
                      RoundedInfoTile(
                          title: "Packaging",
                          value: donation.packagingNeed ? "Yes" : "No"),
                      SizedBox(height: 20),
                      Container(
                        width: double.infinity,
                        child: RaisedButton(
                            child: Text(
                              'Accept',
                              style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.bold,
                                color: Color.fromARGB(255, 229, 41, 62),
                              ),
                            ),
                            color: Colors.white,
                            padding: EdgeInsets.symmetric(vertical: 15),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(30)),
                            onPressed: () {
                              showDialog(
                                  context: context,
                                  builder: (BuildContext bc) {
                                    return AlertDialog(
                                      backgroundColor:
                                          Color.fromARGB(255, 229, 41, 62),
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(20)),
                                      title: Text('Confirmation'),
                                      content: Text(
                                          "Would you like to accept the request?"),
                                      actions: <Widget>[
                                        FlatButton(
                                          child: Text(
                                            "Yes",
                                            style: TextStyle(
                                              color: Colors.black87,
                                            ),
                                          ),
                                          onPressed: () {
                                            updateDonation(donation,
                                                donationStatus.ACCEPTED,


                                                bc);
                                          },
                                        ),
                                        FlatButton(
                                          child: Text(
                                            "No",
                                            style: TextStyle(
                                              color: Colors.black87,
                                            ),
                                          ),
                                          onPressed: () {
                                            Navigator.pop(bc);
                                          },
                                        )
                                      ],
                                    );
                                  });
                            }),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Container(
                        width: double.infinity,
                        child: RaisedButton(
                          child: Text(
                            "Cancel",
                            style: TextStyle(
                                fontSize: 18,
                                color: Color.fromARGB(255, 229, 41, 62),
                                fontWeight: FontWeight.bold),
                          ),
                          color: Colors.black87,
                          padding: EdgeInsets.symmetric(vertical: 15),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(30)),
                          onPressed: () {
                            showDialog(
                                context: context,
                                builder: (BuildContext bc) {
                                  return AlertDialog(
                                    backgroundColor: Colors.white,
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(20)),
                                    title: Text('Confirmation'),
                                    content: Text(
                                        "Would you like to delete the request?"),
                                    actions: <Widget>[
                                      FlatButton(
                                        child: Text(
                                          "Yes",
                                          style: TextStyle(
                                            color: Colors.black87,
                                          ),
                                        ),
                                        onPressed: () {
                                          updateDonation(donation, donationStatus.DECLINED, bc);
                                        },
                                      ),
                                      FlatButton(
                                        child: Text(
                                          "No",
                                          style: TextStyle(
                                            color: Colors.black87,
                                          ),
                                        ),
                                        onPressed: () {
                                          Navigator.pop(bc);
                                        },
                                      )
                                    ],
                                  );
                                });
                            //Navigator.pushNamed(context, '/donorHome');
                          },
                        ),
                      ),
                    ],
                  );
                } else {
                  return Center(child: CircularProgressIndicator());
                }
              },
            ),
            /**/
          ],
        ),
      ),
    );
  }
}

class DoubleRoundedInfoTile extends StatelessWidget {
  final String title1;
  final String value1;
  final String value2;

  DoubleRoundedInfoTile({this.title1, this.value1, this.value2});

  @override
  Widget build(BuildContext context) {
    return Column(children: <Widget>[
      SizedBox(
        height: 5,
      ),
      SizedBox(
        width: double.infinity,
        child: Container(
          padding: EdgeInsets.only(left: 25),
          child: Text(title1,
              style: TextStyle(
                color: Colors.black87,
                fontSize: 20,
              )),
        ),
      ),
      SizedBox(height: 5),
      Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Expanded(
            child: Padding(
              padding: EdgeInsets.only(right: 10),
              child: Container(
                height: 48,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(30),
                  color: Colors.white,
                ),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 30),
                      child: Text(
                        value1,
                        style: TextStyle(
                          fontSize: 18,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Expanded(
            child: Padding(
              padding: EdgeInsets.only(left: 10),
              child: Container(
                height: 48,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(30),
                  color: Colors.white,
                ),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 30),
                      child: Text(
                        value2,
                        style: TextStyle(
                          fontSize: 18,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          SizedBox(
            height: 5,
          ),
        ],
      ),
    ]);
  }
}
