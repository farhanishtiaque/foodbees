import 'package:flutter/material.dart';
import 'package:foodbees/globals.dart';
import 'package:foodbees/services/FB.dart';
import 'package:foodbees/views/About.dart';
import 'package:foodbees/views/Common/UsersList.dart';
import 'package:foodbees/views/Volunteer/StatusNotification.dart';
import 'package:foodbees/views/Volunteer/VolunteerHome.dart';


// ignore: must_be_immutable
class VolunteerLayout extends StatefulWidget {
	FB fb = FB();

	_VolunteerLayoutState dsl = _VolunteerLayoutState();

	@override
	_VolunteerLayoutState createState() => dsl;
}

class _VolunteerLayoutState extends State<VolunteerLayout> {
	int _selectedIndex = 2;
	List<Widget> actions = [];
	String appTitle = '';

	List<CustomPopupMenu> choices = <CustomPopupMenu>[
		CustomPopupMenu(title: 'Profile', icon: Icons.person, val: 1),
		CustomPopupMenu(title: 'Sign Out', icon: Icons.exit_to_app, val: 2)
	];


	getBody(int i){
		Widget w=VolunteerHome(widget);
		switch (i) {
			case 0:
				setState(() {
					appTitle = 'Notifications';
					w = StatusNotification();
				});
				break;
			case 1:
				setState(() {
					appTitle = 'Messaging';
					w = UsersList();
				});
				break;
			case 2:
				setState(() {
					w = VolunteerHome(widget);
				});
				break;
			case 4:
				setState(() {
					appTitle = 'About';
					w= About();
				});
				break;
		}
		return w;
	}

	@override
	Widget build(BuildContext context) {
		return Scaffold(
				backgroundColor: Colors.white,
				appBar: getAppBar(_selectedIndex),
				body: getBody( _selectedIndex),
				bottomNavigationBar: ClipRRect(
					borderRadius: BorderRadius.only(topLeft: Radius.circular(30), topRight: Radius.circular(30)),
					child: BottomNavigationBar(
						currentIndex: _selectedIndex,
						onTap: (int i) {
							_selectedIndex = i;
							switch (i) {
								case 0:
									setState(() {
										appTitle = 'Notifications';
									});
									break;
								case 1:
									setState(() {
										appTitle = 'Messaging';
									});
									break;
								case 2:
									setState(() {
										//	widget.body = Home(widget);
									});
									break;
								case 3:
									setState(() {
										appTitle = 'Donations';
									});
									break;
								case 4:
									setState(() {
										appTitle = 'About';
									});
									break;
							}
						},
						unselectedItemColor: Colors.black,
						selectedItemColor: Theme
								.of(context)
								.primaryColor,
						items: [
							BottomNavigationBarItem(
								title: Text("Notifications"),
								icon: Icon(Icons.notifications_active),
							),
							BottomNavigationBarItem(
								title: Text("Message"),
								icon: Icon(Icons.chat_bubble_outline),
							),
							BottomNavigationBarItem(
								title: Text("Home"),
								icon: Icon(Icons.home),
							),
							BottomNavigationBarItem(
								title: Text("Request"),
								icon: Icon(Icons.local_mall),
							),
							BottomNavigationBarItem(
								title: Text("About"),
								icon: Icon(Icons.info_outline),
							)
						],
					),
				)
		);
	}

	PreferredSizeWidget getAppBar(int i) {
		if (i != 2) {
			return AppBar(
				title: Text(appTitle, style: TextStyle(
						color: Colors.black
				),),
				backgroundColor: Colors.white,
				actions: <Widget>[
					IconButton(
						color: Colors.black,
						icon: Icon(Icons.people),
						onPressed: () {
							setState(() {
								appTitle = 'User List';
								//widget.body = UsersList();
							});
						},
					),
					PopupMenuButton<CustomPopupMenu>(

						onSelected: (ch) {
							switch (ch.title) {
								case 'Profile':
									break;
								case 'Sign Out':
									widget.fb.auth.signOut().then((v) {
										setState(() {
											Navigator.pushNamed(context, '/');
											loginUser = null;
										});
									});
									break;
							}
						},
						padding: EdgeInsets.symmetric(vertical: 5, horizontal: 0),
						child: Image.network(
								'https://ui-avatars.com/api/?length=2&bold=true&size=200&name=' + loginUser.name),
						itemBuilder: (BuildContext bc) {
							return choices.map((CustomPopupMenu choice) {
								return PopupMenuItem<CustomPopupMenu>(
									value: choice,
									child: Text(choice.title),
								);
							}).toList();
						},
					),
				],
			);
		} else {
			return PreferredSize(
					preferredSize: Size.fromHeight(180), // here the desired height
					child: Container(

						padding: EdgeInsets.all(30),
						child: Column(
								crossAxisAlignment: CrossAxisAlignment.start,
								children: <Widget>[
									Spacer(),
									Row(
										mainAxisAlignment: MainAxisAlignment.spaceBetween,
										children: <Widget>[
											Column(
												children: <Widget>[
													Text('Welcome',
															style: TextStyle(
																	color: Colors.white,
																	fontSize: 35
															)
													),
													Text(loginUser.name.toUpperCase(),
															style: TextStyle(
																	color: Colors.white,
																	fontSize: 35,
																	fontWeight: FontWeight.bold
															)
													)
												],
											),
											Column(
												children: <Widget>[
													FlatButton(
														onPressed: () {
															Navigator.pushNamed(context, '/volunteerProfile');
														},
													  child: Container(
													  		height: 100.0,
													  		width: 100.0,
													  		decoration: new BoxDecoration(
													  			shape: BoxShape.circle,
													  		),
													  		child: ClipRRect(
													  			borderRadius: BorderRadius.circular(75.0),
													  			child: Image.network(
													  					'https://ui-avatars.com/api/?length=2&bold=true&size=200&name=' + loginUser
													  							.name),
													  		)

													  ),
													)
												],
											)
										],
									),
								]),
						decoration: BoxDecoration(
								color: Theme
										.of(context)
										.primaryColor,
								borderRadius: BorderRadius.only(
										bottomLeft: Radius.circular(50),
										bottomRight: Radius.circular(50)
								)
						),
					)
			);
		}
	}


}

class CustomPopupMenu {
	CustomPopupMenu({this.title, this.icon, @required this.val});

	int val;
	String title;
	IconData icon;
}
