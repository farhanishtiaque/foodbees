import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:foodbees/models/Donation.dart';
import 'package:foodbees/services/FB.dart';
import 'package:foodbees/views/Volunteer/VolunteerLayout.dart';
import 'dart:ui' as ui;


import '../../globals.dart';


class VolunteerHome extends StatefulWidget {
	FB fb = FB();

	VolunteerLayout layout;

	VolunteerHome(VolunteerLayout donorLayout) {
		layout = donorLayout;
	}


	@override
	_VolunteerHomeState createState() => _VolunteerHomeState();
}


class _VolunteerHomeState extends State<VolunteerHome> {

	final double _borderRadius =24;
	@override
	Widget build(BuildContext context) {

		return SingleChildScrollView(
		  child: Center(
					child: FutureBuilder(
		  			builder: (bc, snapshot) {
		  				if (snapshot.hasData) {
		  					List<Widget> donationList = [								];
		  					(snapshot.data as List<DocumentSnapshot>).forEach((f) {
		  						Donation donation = Donation.fromJson(f.data);
		  						donationList.add(
		  								Padding(
		  								  padding: const EdgeInsets.all(8.0),
		  								  child: Stack(
		  								    children:<Widget> [

		  								    	Container(
													height: 150,
		  								    	  decoration: BoxDecoration(
														borderRadius: BorderRadius.circular(24),
													gradient: LinearGradient(
														colors: [Color.fromARGB(255, 229, 41, 62),Color.fromARGB(255, 255, 15, 41)],
														begin: Alignment.topLeft,
														end :Alignment.bottomRight,
													),
//														boxShadow:[ BoxShadow(
//															color: Color.fromARGB(255, 229, 41, 62),
//															blurRadius: 2,
//															offset: Offset(0,6),
//														)
//
//														] 	,
													),
		  								    	  child: Column(
		  								    	  	mainAxisSize: MainAxisSize.min,
		  								    	  	children: <Widget>[
		  								    	  		ListTile(
		  								    	  			title: Center(
		  								    	  				child: Text(
		  								    	  					donation.food,
		  								    	  					textAlign: TextAlign.center,
		  								    	  					overflow: TextOverflow.ellipsis,
		  								    	  					style: TextStyle(
		  								    	  						color: Colors.white,
		  								    	  						fontWeight: FontWeight.bold,
		  								    	  						fontSize: 25,
		  								    	  					),
		  								    	  				),
		  								    	  			),
		  								    	  			subtitle: Center(
		  								    	  				child: Text(
		  								    	  					donation.location,
		  								    	  					style: TextStyle(
		  								    	  						color: Colors.black,
		  								    	  						fontSize: 18,
		  								    	  					),
		  								    	  				),
		  								    	  			),
		  								    	  			isThreeLine: false,

		  								    	  		),
		  								    	  		ButtonBar(
		  								    	  			alignment: MainAxisAlignment.center,
		  								    	  			children: <Widget>[
		  								    	  				FlatButton(
		  								    	  					color: Colors.white,
		  								    	  					shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
		  								    	  					child: Padding(
		  								    	  						padding: const EdgeInsets.all(12.0),
		  								    	  						child: const Text('Details'),
		  								    	  					),
		  								    	  					onPressed: () {
		  								    	  						Navigator.pushNamed(context, '/donationDetails',
		  								    	  								arguments:{'donation': donation}
		  								    	  						);
		  								    	  					},
		  								    	  				),
		  								    	  				FlatButton(
		  								    	  					color: Colors.white,
		  								    	  					shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
		  								    	  					child: Padding(
		  								    	  						padding: const EdgeInsets.all(12.0),
		  								    	  						child: const Text('Cancel'),
		  								    	  					),
		  								    	  					onPressed: () { /* ... */ },
		  								    	  				),
		  								    	  			],
		  								    	  		),
		  								    	  	],
		  								    	  ),
		  								    	),
														Positioned(
															right: 0,
															bottom: 0,
															top: 0,
															child: CustomPaint(
																size: Size(100, 150),
																painter: CustomCardShapePainter(_borderRadius,Color.fromARGB(255, 229, 41, 62),Color.fromARGB(255, 255, 15, 41)),
															),
														),

									],

		  								  ),
		  								)
		  						);
		  					});
		  					return Column(
		  						children: donationList,
		  					);
		  				} else {
		  					return CircularProgressIndicator();
		  				}
		  			},
		  			future: widget.fb.store.collection(Collections.donations).where('status',isEqualTo: donationStatus.POSTED.index).getDocuments().then((v) {
		  				return v.documents;
		  			}),
		  		)
		  ),
		);
	}

}


class CustomCardShapePainter extends CustomPainter {
	final double radius;
	final Color startColor;
	final Color endColor;

	CustomCardShapePainter(this.radius, this.startColor, this.endColor);

	@override
	void paint(Canvas canvas, Size size) {
		var radius = 24.0;

		var paint = Paint();
		paint.shader = ui.Gradient.linear(
				Offset(0, 0), Offset(size.width, size.height), [
			HSLColor.fromColor(startColor).withLightness(0.8).toColor(),
			endColor
		]);

		var path = Path()
			..moveTo(0, size.height)
			..lineTo(size.width - radius, size.height)
			..quadraticBezierTo(
					size.width, size.height, size.width, size.height - radius)
			..lineTo(size.width, radius)
			..quadraticBezierTo(size.width, 0, size.width - radius, 0)
			..lineTo(size.width - 1.5 * radius, 0)
			..quadraticBezierTo(-radius, 2 * radius, 0, size.height)
			..close();

		canvas.drawPath(path, paint);
	}

	@override
	bool shouldRepaint(CustomPainter oldDelegate) {
		return true;
	}
}