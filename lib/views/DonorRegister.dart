import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:foodbees/models/User.dart';
import 'package:foodbees/services/FB.dart';

// ignore: must_be_immutable
class DonorRegister extends StatefulWidget {
	FB fb = FB();

	@override
	_DonorRegisterState createState() => _DonorRegisterState();
}

class _DonorRegisterState extends State<DonorRegister> {

	Widget indicator = Text("Sign Up",
		style: TextStyle(
			color: Colors.white,
			fontSize: 18,
			fontWeight: FontWeight.bold,
		),);
	String userType;

	@override
	Widget build(BuildContext context) {
		TextEditingController name = new TextEditingController();
		TextEditingController institution = new TextEditingController();
		TextEditingController occupation = new TextEditingController();
		TextEditingController mobile = new TextEditingController();
		TextEditingController email = new TextEditingController();
		TextEditingController password = new TextEditingController();
		return Scaffold(
			backgroundColor: Colors.white,
			body: SingleChildScrollView(
				padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
				child: Column(
					mainAxisAlignment: MainAxisAlignment.center,
					children: <Widget>[
						SizedBox(
							height: 100,
						),
						Container(
							alignment: Alignment.topLeft,
							padding: EdgeInsets.only(left: 15),
							child: Text(
								'Sign Up',
								style: TextStyle(
										color: Color.fromARGB(255, 229, 41, 62),
										fontWeight: FontWeight.bold,
										fontSize: 33),
							),
						),
						SizedBox(
							height: 10,
						),
						Container(
							height: 48,
							decoration: BoxDecoration(
								borderRadius: BorderRadius.circular(30),
								color: Color.fromARGB(255, 231, 235, 237),
							),
							child: TextFormField(
								controller: name,
								decoration: InputDecoration(
									labelText: 'Name',
									border: InputBorder.none,
									contentPadding: EdgeInsets.symmetric(horizontal: 20),
								),
							),
						),
						SizedBox(
							height: 10,
						),
						Container(
							height: 48,
							decoration: BoxDecoration(
								borderRadius: BorderRadius.circular(30),
								color: Color.fromARGB(255, 231, 235, 237),
							),
							child: TextFormField(
								controller: institution,
								decoration: InputDecoration(
									labelText: 'Institution',
									border: InputBorder.none,
									contentPadding: EdgeInsets.symmetric(horizontal: 20),
								),
							),
						),
						SizedBox(
							height: 10,
						),
						Container(
							height: 48,
							decoration: BoxDecoration(
								borderRadius: BorderRadius.circular(30),
								color: Color.fromARGB(255, 231, 235, 237),
							),
							child: TextFormField(
								controller: occupation,
								decoration: InputDecoration(
									labelText: "Ocupation",
									border: InputBorder.none,
									contentPadding: EdgeInsets.symmetric(horizontal: 20),
								),
							),
						),
						SizedBox(
							height: 10,
						),

						Container(
							height: 48,
							decoration: BoxDecoration(
								borderRadius: BorderRadius.circular(30),
								color: Color.fromARGB(255, 231, 235, 237),
							),
							child: TextFormField(
								controller: mobile,
								keyboardType: TextInputType.numberWithOptions(
										decimal: true, signed: true),
								decoration: InputDecoration(
									labelText: 'Mobile',
									border: InputBorder.none,
									contentPadding: EdgeInsets.symmetric(horizontal: 20),
								),
							),
						),
						SizedBox(
							height: 10,
						),
						Container(
							height: 48,
							decoration: BoxDecoration(
								borderRadius: BorderRadius.circular(30),
								color: Color.fromARGB(255, 231, 235, 237),
							),
							child: TextFormField(
								controller: email,
								keyboardType: TextInputType.emailAddress,
								decoration: InputDecoration(
									labelText: 'Email',
									border: InputBorder.none,
									contentPadding: EdgeInsets.symmetric(horizontal: 20),
								),
							),
						),
						SizedBox(
							height: 10,
						),
						Container(
							height: 48,
							decoration: BoxDecoration(
								borderRadius: BorderRadius.circular(30),
								color: Color.fromARGB(255, 231, 235, 237),
							),
							child: TextFormField(
								controller: password,
								obscureText: true,
								decoration: InputDecoration(
									hintText: "Password",
									border: InputBorder.none,
									contentPadding: EdgeInsets.symmetric(horizontal: 20),
								),
							),
						),
						SizedBox(height: 10,),
						Container(
							width: double.infinity,

						  child: RaisedButton(
						  	child: indicator,
									padding: EdgeInsets.symmetric(vertical: 15),
									color: Color.fromARGB(255, 229, 41, 62),

									shape: new RoundedRectangleBorder(
						  	borderRadius: new BorderRadius.circular(30)
						  ),
						  	onPressed: () {
						  		User user = User(
						  			userType: "Donor",
						  			name: name.text,
						  			institution: institution.text,
						  			occupation: occupation.text,
						  			mobile: mobile.text,
						  			email: email.text
						  		);
						  		widget.fb.auth.createUserWithEmailAndPassword(
						  			email: user.email, password: password.text).then((AuthResult ar) {
						  			UserUpdateInfo uui = UserUpdateInfo();
						  			uui.displayName = user.name;
						  			ar.user.updateProfile(uui).then((uuiRes) {
						  				widget.fb.store.collection(Collections.users)
						  					.add(user.toJson()).then((res) {
						  					user.id = res.documentID;
						  					res.updateData(user.toJson()).then((onValue) {
						  						setState(() {
						  							Scaffold.of(context)
						  								.showSnackBar(
						  								SnackBar(content: Text('Registered!'),
						  									backgroundColor: Colors.green)
						  							);
						  						});
						  					});
						  				});
						  			});
						  		});
						  		setState(() {
						  			indicator = Text("Registered!");
						  		});
						  		//});
						  	}
						  ),
						),
					],
				),
			),
		);
	}
}
