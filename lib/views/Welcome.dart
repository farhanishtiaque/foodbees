import 'package:flutter/material.dart';
import 'package:foodbees/widget/app_background.dart';

class Welcome extends StatefulWidget {
  @override
  _WelcomeState createState() => _WelcomeState();
}

class _WelcomeState extends State<Welcome> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //backgroundColor: Color.fromARGB(255, 229, 41, 62),
      body: Stack(
        children: <Widget>[
          AppBackground(),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                'Welcome to FOODBees',
                style: TextStyle(fontSize: 33),
              ),
              Text("Sign Up or Login as"),
              SizedBox(height: 10,),
              Container(
                height: 48,
                width: double.infinity,
                padding: EdgeInsets.symmetric( horizontal: 20),
                child: RaisedButton(
                  color: Colors.white,
                  // padding: EdgeInsets.symmetric(vertical: 15),
                  shape: RoundedRectangleBorder(
                    side: BorderSide(
                      width: 2,
                      color: Color.fromARGB(255, 229, 41, 62),
                    ),
                    borderRadius: BorderRadius.circular(30),
                  ),
                  child: Text(
                    "Donor",

                    style: TextStyle(fontSize: 28,
                      color:  Color.fromARGB(255, 229, 41, 62),
                      //fontWeight: FontWeight.bold,
                    ),
                  ),
                  onPressed: (){
                    Navigator.pushNamed(context, '/donorLogin');
                  },
                ),
              ),
              SizedBox(height: 10,),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 20),
                width: double.infinity,
                height: 48,
                child: RaisedButton(
                  color: Color.fromARGB(255, 229, 41, 62),
                  // padding: EdgeInsets.symmetric(vertical: 15),
                  shape: RoundedRectangleBorder(

                    borderRadius: BorderRadius.circular(30),
                  ),
                  child: Text(
                    "Volunteer",
                    style: TextStyle(fontSize: 28,
                      color:  Colors.white,
                      //fontWeight: FontWeight.bold,
                    ),
                  ),
                  onPressed: (){
                    Navigator.pushNamed(context, '/volunteerLogin');
                  },
                ),
              ),
            ],
          ),
        ],

      ),
    );
  }
}
