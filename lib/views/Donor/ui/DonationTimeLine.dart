import 'package:flutter/material.dart';
import 'package:foodbees/globals.dart';
import 'package:foodbees/models/Donation.dart';
import 'package:foodbees/services/FB.dart';
import 'package:intl/intl.dart';
import 'package:timeline_tile/timeline_tile.dart';

class DonationTimeLine extends StatefulWidget {
  @override
  _DonationTimeLineState createState() => _DonationTimeLineState();
}

class _DonationTimeLineState extends State<DonationTimeLine> {
  Donation donation;

  @override
  Widget build(BuildContext context) {
    if (ModalRoute.of(context).settings.arguments != null) {
      donation = (ModalRoute.of(context).settings.arguments as Map)['donation'];
    }
    return Container(

      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 80),
      color: Colors.white,

      child: Column(
//        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Container(

            child: Column(
              children: <Widget>[
                Text(
                  'Donation Timeline',

//                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.black87,
                      fontSize: 22, fontWeight: FontWeight.bold),


                ),
//                Text(
//                  donation.location,
////                  textAlign: TextAlign.center,
//                  style: TextStyle(color: Colors.black87,
//                      fontSize: 16),
//                ),
              ],
            )
          ),

          TimelineTile(
            alignment: TimelineAlign.manual,
            lineX: 0.1,
            isFirst: true,
            indicatorStyle: IndicatorStyle(
              width: 20,
              color: Color.fromARGB(255, 229, 41, 62),
            ),
            topLineStyle: LineStyle(
              color: Colors.black87,
              width: 6,
            ),
            rightChild: _Child(
              text: 'Pending',
              time: donation.postTime,
              donation: donation,
            ),
          ),
          const TimelineDivider(
            begin: 0.1,
            end: 0.9,
            thickness: 6,
            color: Colors.black87,
          ),
          TimelineTile(
            alignment: TimelineAlign.manual,
            lineX: 0.9,
            topLineStyle: LineStyle(
              color: Colors.black87,
              width: 6,
            ),
            bottomLineStyle: LineStyle(
              color: Colors.black87,
              width: 6,
            ),
            indicatorStyle: IndicatorStyle(
              width: 20,
              color:Color.fromARGB(255, 229, 41, 62),
            ),
            leftChild: _Child(
              text: 'Accepted',
              time: donation.acceptedTime,
              donation: donation,
            ),
          ),
          const TimelineDivider(
            begin: 0.1,
            end: 0.9,
            thickness: 6,
            color: Colors.black87,
          ),
          TimelineTile(
            alignment: TimelineAlign.manual,
            lineX: 0.1,
            topLineStyle: LineStyle(
              color: Colors.black87,
              width: 6,
            ),
            indicatorStyle: IndicatorStyle(
              width: 20,
              color: Color.fromARGB(255, 229, 41, 62),
            ),
            rightChild: _Child(
              text: 'Picked',
              donation: donation,
              time: donation.pickedTime,
            ),
          ),
          const TimelineDivider(
            begin: 0.1,
            end: 0.9,
            thickness: 6,
            color: Colors.black87,
          ),
          TimelineTile(
            isLast: true,
            alignment: TimelineAlign.manual,
            lineX: 0.9,
            topLineStyle: LineStyle(
              color: Colors.black87,
              width: 6,
            ),
            indicatorStyle: IndicatorStyle(
              width: 20,
              color: Color.fromARGB(255, 229, 41, 62),
            ),
            leftChild: _Child(
              text: 'Delivered',
              donation: donation,
              time: donation.deliveryTime,
            ),
          )
        ],
      ),
    );
  }
}

class _Child extends StatelessWidget {
  String text = "";
  DateTime time = null;
  Donation donation = null;

  _Child({Key key, @required String text, @required Donation donation, DateTime time}) : super(key: key) {
    this.text = text;
    this.time = time;
    this.donation = donation;
  }

  FB fb = FB();

  updateDonation(donationStatus ds, String field, BuildContext bc) {
    fb.store
        .collection(Collections.donations)
        .document(donation.id)
        .updateData({'status': ds.index, field: DateTime.now()}).then((r) {
      Navigator.pop(bc);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(30),
          color: Color.fromARGB(255, 229, 41, 62)
        ),
        margin: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
        padding: EdgeInsets.symmetric(horizontal: 0, vertical: 10),
//        color: Color.fromARGB(255, 229, 41, 62),
        child: MaterialButton(
          onPressed: () {
            if (donation.status == donationStatus.ACCEPTED.index && loginUser.userType == 'Donor') {
              showDialog(
                  context: context,
                  builder: (BuildContext bc) {
                    return AlertDialog(
                      backgroundColor: Color.fromARGB(255, 229, 41, 62),
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
                      title: Text('Confirmation'),
                      content: Text("Is donation picked?"),
                      actions: <Widget>[
                        FlatButton(
                          child: Text(
                            "Yes",
                            style: TextStyle(
                              color: Colors.black87,
                            ),
                          ),
                          onPressed: () {
                            updateDonation(donationStatus.PICKED, 'pickedTime', bc);
                          },
                        ),
                        FlatButton(
                          child: Text(
                            "No",
                            style: TextStyle(
                              color: Colors.black87,
                            ),
                          ),
                          onPressed: () {
                            Navigator.pop(bc);
                          },
                        )
                      ],
                    );
                  });

            }
            if (loginUser.userType == 'Volunteer') {
              if (donation.status == donationStatus.PICKED.index) {
                showDialog(
                    context: context,
                    builder: (BuildContext bc) {
                      return AlertDialog(
                        backgroundColor: Color.fromARGB(255, 229, 41, 62),
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
                        title: Text('Confirmation'),
                        content: Text("Would you like to deliver the request?"),
                        actions: <Widget>[
                          FlatButton(
                            child: Text(
                              "Yes",
                              style: TextStyle(
                                color: Colors.black87,
                              ),
                            ),
                            onPressed: () {
                              updateDonation(donationStatus.DELIVERED, 'deliveryTime', bc);
                            },
                          ),
                          FlatButton(
                            child: Text(
                              "No",
                              style: TextStyle(
                                color: Colors.black87,
                              ),
                            ),
                            onPressed: () {
                              Navigator.pop(bc);
                            },
                          )
                        ],
                      );
                    });
              }
            }
          },
          child: Column(
            children: <Widget>[
              Text(
                this.text,
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.w800),
              ),
              Text(
                time != null ? DateFormat('d MMM, y h:m a').format(time) : '',
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 16),
              )
            ],
          ),
        ));
  }
}
