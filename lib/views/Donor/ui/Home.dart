import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:foodbees/globals.dart';
import 'package:foodbees/models/Donation.dart';
import 'package:foodbees/services/FB.dart';
import 'package:foodbees/views/Donor/DonorLayout.dart';

// ignore: must_be_immutable
class Home extends StatefulWidget {
  FB fb = FB();
  DonorLayout layout;

  Home(DonorLayout donorLayout) {
    layout = donorLayout;
  }

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Center(
          //constraints: BoxConstraints(maxHeight: 1000),
          child: FutureBuilder(
            builder: (bc, snapshot) {
              if (snapshot.hasData) {
                List<Widget> donationList = [];
                (snapshot.data as List<DocumentSnapshot>).forEach((f) {
                  Donation donation = Donation.fromJson(f.data);
                  donationList.add(
                      Ink(
                        child: Card(
                    child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          ListTile(

                            title: Center(
                              child: Text(
                                donation.food,
                                textAlign: TextAlign.center,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                  color: Color.fromARGB(255, 229, 41, 62),
                                  fontWeight: FontWeight.bold,
                                  fontSize: 25,
                                ),
                              ),
                            ),
                            subtitle: Center(
                              child: Text(
                                donation.location,
                                style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 18,
                                ),
                              ),
                            ),
                            isThreeLine: false,
                          ),
                          ButtonBar(
                            alignment: MainAxisAlignment.center,
                            children: <Widget>[
                              FlatButton(
                                color: Color.fromARGB(255, 229, 41, 62),
                                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
                                child: Padding(
                                  padding: const EdgeInsets.all(12.0),
                                  child: const Text('Details'),
                                ),
                                onPressed: () {
                                  Navigator.pushNamed(context, '/donationDetails', arguments: {'donation': donation});
                                },
                              ),
                              FlatButton(
                                color: Color.fromARGB(255, 229, 41, 62),
                                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
                                child: Padding(
                                  padding: const EdgeInsets.all(12.0),
                                  child: const Text('Cancel'),
                                ),
                                onPressed: () {
                                  /* ... */
                                },
                              ),
                            ],
                          ),
                        ],
                    ),
                  ),
                      )
                  );
                });
                return Column(

                  children: donationList,
                );
              } else {
                return CircularProgressIndicator();
              }
            },
            future: widget.fb.store
                .collection(Collections.donations)
                .where('donorID', isEqualTo: loginUser.id)
                .getDocuments()
                .then((v) {
              return v.documents;
            }),
          )),
    );
  }
}
