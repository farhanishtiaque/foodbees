import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:foodbees/globals.dart';
import 'package:foodbees/models/Donation.dart';
import 'package:foodbees/services/FB.dart';
import 'package:foodbees/views/Donor/DonorLayout.dart';

// ignore: must_be_immutable
class AcceptedNotification extends StatefulWidget {
  FB fb = FB();
  DonorLayout layout;

  AcceptedNotification(DonorLayout donorLayout) {
    layout = donorLayout;
  }

  @override
  _AcceptedNotificationState createState() => _AcceptedNotificationState();
}

class _AcceptedNotificationState extends State<AcceptedNotification> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      shrinkWrap: true,
      padding: EdgeInsets.symmetric(horizontal: 2, vertical: 5),
      children: <Widget>[
        Center(

            child: FutureBuilder(
              builder: (bc, snapshot) {
                if (snapshot.hasData) {
                  List<Widget> donationList = [];
                  (snapshot.data as List<DocumentSnapshot>).forEach((f) {
                    Donation donation = Donation.fromJson(f.data);
                    donationList.add(Ink(

                      child: Card(
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            ListTile(
                              title: Center(
                                child: Text(
                                  donation.food,
                                  textAlign: TextAlign.center,
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(
                                    color: Colors.redAccent,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 25,
                                  ),
                                ),
                              ),
                              subtitle: Center(
                                child: Text(
                                  donation.location,
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 18,
                                  ),
                                ),
                              ),
                              isThreeLine: false,
                            ),
                            ButtonBar(
                              alignment: MainAxisAlignment.center,
                              children: <Widget>[
                                FlatButton(
                                  color: Color.fromARGB(255, 229, 41, 62),
                                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
                                  child: Padding(
                                    padding: const EdgeInsets.all(12.0),
                                    child: const Text('Details'),
                                  ),
                                  onPressed: () {
                                    Navigator.pushNamed(context, '/donation', arguments: {'donation': donation});
                                  },
                                ),
                                FlatButton(
                                  color: Color.fromARGB(255, 229, 41, 62),
                                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
                                  child: Padding(
                                    padding: const EdgeInsets.all(12.0),
                                    child: const Text('Cancel'),
                                  ),
                                  onPressed: () {
                                    /* ... */
                                  },
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ));
                  });
                  return Column(
                    children: donationList,
                  );
                } else {
                  return CircularProgressIndicator();
                }
              },
              future: widget.fb.store
                  .collection(Collections.donations)
                  .where('donorID', isEqualTo: loginUser.id)
                  .getDocuments()
                  .then((v) {
                return v.documents;
              }),
            ))
      ],
    );
  }
}
