import 'package:flutter/material.dart';


class DonationState extends StatefulWidget {
  @override
  _DonationStateState createState() => _DonationStateState();
}

class _DonationStateState extends State<DonationState> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      /*appBar: AppBar(
        title: Text('DonorHome'),
      ),*/
      backgroundColor: Color.fromARGB(255, 229, 41, 62),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Center(
            child: Column(
              children: <Widget>[
                Text(
                  "Donation States",
                  style: TextStyle(
                    fontSize: 30,
                    color: Colors.white,
                  ),
                ),
                SizedBox(
                  height: 30,
                ),
                StateButton(
                  btnName: "Pending",
                  function: () {},
                ),
                StateButton(
                  btnName: "Accepted",
                  function: () {},
                ),
                StateButton(
                  btnName: "Picked Up",

                  function: () {
                    
                  },
                ),
                StateButton(
                  btnName: "Donated",
                  function: () {},
                ),
//                StateButton(
//                  btnName: "Completed",
//                  function: () {},
//                ),
                SizedBox(
                  height: 30,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class StateButton extends StatelessWidget {
  final String btnName;
  final Function function;

  StateButton({this.btnName, this.function});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 200,
      margin: EdgeInsets.all(5),
      child: RaisedButton(
        color: Colors.black,
        padding: EdgeInsets.all(15),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(30),
        ),
        child: Text(
          btnName,
          style: TextStyle(
            fontSize: 18,
            color: Colors.white,
            fontWeight: FontWeight.bold,
          ),
        ),
        onPressed: function,
      ),
    );
  }
}
