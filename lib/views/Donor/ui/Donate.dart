import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:foodbees/globals.dart';
import 'package:foodbees/models/Donation.dart';
import 'package:foodbees/models/User.dart';
import 'package:foodbees/services/FB.dart';
import 'package:intl/intl.dart';

// ignore: must_be_immutable
class Donate extends StatefulWidget {
	FB fb = FB();

	@override
	_DonateState createState() => _DonateState();
}

class _DonateState extends State<Donate> {
	DateTime _date = DateTime.now();
	TimeOfDay _time = TimeOfDay.now();
	bool needPackaging = true;
	TextEditingController food = new TextEditingController();
	TextEditingController location = new TextEditingController();
	String _selectedTime = '';
	String _selectedDate = '';
	String _volunteer = '';
	TextEditingController  amount = new TextEditingController();




	Future<Null> _selectDate(BuildContext context) async {
		final DateTime picked = await showDatePicker(
			context: context,
			initialDate: _date,
			firstDate: DateTime(_date.year, _date.month, _date.day),
			lastDate: DateTime(_date.year, _date.month + 1, _date.day));
		if (picked != null && picked != _date)
			setState(() {
				_date = picked;
				_selectedDate = DateFormat('dd MMM, YYYY').format(DateTime(_date.year, _date.month, _date.day));
			});
	}

	Future<Null> _selectTime(BuildContext context) async {
		final TimeOfDay picked = await showTimePicker(
			initialTime: _time,
			context: context
		);
		if (picked != null && picked != _time)
			setState(() {
				_time = picked;
				_selectedTime = DateFormat('hh:mm a').format(DateTime(_date.year, _date.month, _date.day,
					_time.hour, _time.minute));
			});
	}

	@override
	Widget build(BuildContext context) {
		_selectedDate = DateFormat('dd MMM, yyyy').format(DateTime(_date.year, _date.month, _date.day));
		_selectedTime =
			DateFormat('hh:mm a').format(DateTime(_date.year, _date.month, _date.day, _time.hour, _time.minute));
		return Form(
			child: ListView(
				padding: EdgeInsets.all(20),
				children: <Widget>[
					Column(children: <Widget>[
						SizedBox(
							width: double.infinity,
							child: Container(
								padding: EdgeInsets.only(left: 10),
								child: Text("Foods",
									style: TextStyle(
										color: Colors.black87,
										fontSize: 20,
									)),
							),
						),
						SizedBox(height: 5),
						Container(
							decoration: BoxDecoration(
								borderRadius: BorderRadius.circular(30),
								color: Colors.white,
							),
							child: TextFormField(
								controller: food,
								keyboardType: TextInputType.multiline,
								maxLines: null,
								decoration: InputDecoration(
									border: InputBorder.none,
									contentPadding: EdgeInsets.only(left: 30),
									hintText: "Ex: Briyani,Vat,Cake,Biskits"),
							),
						),
					]),
					SizedBox(height: 10),
					Column(children: <Widget>[
						SizedBox(
							width: double.infinity,
							child: Container(
								padding: EdgeInsets.only(left: 10),
								child: Text("Amount",
										style: TextStyle(
											color: Colors.black87,
											fontSize: 20,
										)),
							),
						),
						SizedBox(height: 5),
						Container(
							decoration: BoxDecoration(
								borderRadius: BorderRadius.circular(30),
								color: Colors.white,
							),
							child: TextFormField(
								controller: amount,
								keyboardType: TextInputType.multiline,
								maxLines: null,
								decoration: InputDecoration(
										border: InputBorder.none,
										contentPadding: EdgeInsets.only(left: 30),
										hintText: "2 hari,2 kg"),
							),
						),
					]),
					SizedBox(height: 10),
					Column(children: <Widget>[
						SizedBox(
							width: double.infinity,
							child: Container(
								padding: EdgeInsets.only(left: 10),
								child: Text("Location",
									style: TextStyle(
										color: Colors.black87,
										fontSize: 20,
									)),
							),
						),
						SizedBox(height: 5),
						Container(
							decoration: BoxDecoration(
								borderRadius: BorderRadius.circular(30),
								color: Colors.white,
							),
							child: TextFormField(
								controller: location,
								decoration: InputDecoration(
									border: InputBorder.none,
									contentPadding: EdgeInsets.only(left: 30),
									hintText: "390/A, C-Block, BashunDhara"
								),
							),
						),
					]),
					SizedBox(height: 10),
					Column(children: <Widget>[
						SizedBox(
							width: double.infinity,
							child: Container(
								padding: EdgeInsets.only(left: 10),
								child: Text("Pick up time",
									style: TextStyle(
										color: Colors.black87,
										fontSize: 20,
									)),
							),
						),
						SizedBox(height: 5),
						Row(
							mainAxisSize: MainAxisSize.min,
							children: <Widget>[
								Expanded(
									child: Padding(
										padding: EdgeInsets.only(right: 10),
										child: Container(
											decoration: BoxDecoration(
												borderRadius: BorderRadius.circular(30),
												color: Colors.white,
											),
											child: TextField(
												onTap: () {
													_selectDate(context);
												},
												readOnly: true,
												decoration: InputDecoration(
													border: InputBorder.none,
													contentPadding: EdgeInsets.only(left: 30),
													hintText: _selectedDate
												),
											),
										),
									),
								),
								Expanded(
									child: Padding(
										padding: EdgeInsets.only(left: 10),
										child: Container(
											decoration: BoxDecoration(
												borderRadius: BorderRadius.circular(30),
												color: Colors.white,
											),
											child: TextFormField(
												onTap: () {
													_selectTime(context);
												},
												readOnly: true,
												//controller: _selectedTime,
												decoration: InputDecoration(
													border: InputBorder.none,
													contentPadding: EdgeInsets.only(left: 30),
													hintText: _selectedTime
												),
											),
										),
									),
								),
							],
						),
					]),
					SizedBox(height: 10),
					Column(children: <Widget>[
						SizedBox(
							width: double.infinity,
							child: Container(
								padding: EdgeInsets.only(left: 10),
								child: Text("Package Needed",
									style: TextStyle(
										color: Colors.black87,
										fontSize: 20,
									)),
							),
						),
						SizedBox(height: 5),
						Container(
							padding: EdgeInsets.only(left: 20),
							decoration: BoxDecoration(
								borderRadius: BorderRadius.circular(30),
								color: Colors.white,
							),
							child:
							DropdownButtonHideUnderline(
								child: DropdownButton(
									isExpanded: true,
									hint: Text('Select One'),
									items: [
										DropdownMenuItem(
											child: Text('Yes'),
											value: true,
										),
										DropdownMenuItem(
											child: Text('No'),
											value: false,
										)
									],
									onChanged: (e) {
										setState(() {
											needPackaging = e;
										});
									},
									value: needPackaging,
								),
							)
						)
					]),
					SizedBox(height: 10),
					Column(children: <Widget>[
						SizedBox(
							width: double.infinity,
//							child: Container(
//								padding: EdgeInsets.only(left: 10),
//								child: Text("Volunteer",
//									style: TextStyle(
//										color: Color.fromARGB(255, 139, 148, 165),
//										fontSize: 20,
//									)),
//							),
						),
//						SizedBox(height: 5),
//						Container(
//							padding: EdgeInsets.only(left: 20),
//							decoration: BoxDecoration(
//								borderRadius: BorderRadius.circular(30),
//								color: Color.fromARGB(255, 231, 235, 237),
//							),
//							child: DropdownButtonHideUnderline(
//								child: FutureBuilder(
//									future: widget.fb.store.collection(Collections.users).where('userType',
//										isEqualTo: 'Volunteer').getDocuments().then((v) {
//										return v.documents;
//									}),
//									builder: (context, snapshot) {
//										User selectedUser;
//										if (snapshot.hasError) {
//											return new Container();
//										} else if (snapshot.hasData) {
//											List<DropdownMenuItem<User>> userList = [];
//											(snapshot.data as List<DocumentSnapshot>).forEach((f) {
//												User volunteer = User.fromJson(f.data);
//												userList.add(DropdownMenuItem(
//													child: Text(volunteer.name),
//													value: volunteer,
//												));
//											});
//											return DropdownButton(
//												value: selectedUser,
//												isExpanded: true,
//												items: userList,
//												hint: Text('Choose Volunteer'),
//												onChanged: (User selected) {
//													selectedUser = selected;
//													_volunteer = selected.id;
//												},
//											);
//										} else {
//											return CircularProgressIndicator();
//										}
//									},
//								),
//							)
//						),
					]),
					SizedBox(height: 10,),
					RaisedButton(
						child: Text('Post Donation',
							style: TextStyle(
								fontSize: 18,
								color: Color.fromARGB(255, 229, 41, 62),
								fontWeight: FontWeight.bold ,
							),
						),
						color: Colors.white,
						padding: EdgeInsets.symmetric(vertical: 15),
						shape: RoundedRectangleBorder(
							borderRadius: BorderRadius.circular(30)
						),
						onPressed: () {
							showDialog(context: context, builder: (BuildContext bc) {
								return AlertDialog(
									shape: RoundedRectangleBorder(
										borderRadius: BorderRadius.circular(20)
									),
									title: Text('Confirmation'),
									content: Text("Would you line to post the donation?"),
									actions: <Widget>[
										FlatButton(
											child: Text("Yes"),
											onPressed: () {
												Donation donation = Donation(
													food: food.text,
													location: location.text,
													packagingNeed: needPackaging,
													pickUpDate: _date,
													pickUpTime: _selectedTime,
													postTime: DateTime.now(),
													status: donationStatus.POSTED.index,
													//volunteer: _volunteer,
													donorID: loginUser.id,
													amount: amount.text
												);
												widget.fb.store.collection(Collections.donations).add(donation.toJson()).then((v) {
													donation.id = v.documentID;
													v.updateData(donation.toJson()).then((c) {
														Navigator.pop(bc);
														Scaffold.of(context).showSnackBar(SnackBar(
															content: Text('Posted!'),
														));
													});
												});
											},
										),
										FlatButton(
											child: Text("No"),
											onPressed: () {
												Navigator.pop(bc);
											},
										)
									],
								);
							});
						}
					),
				],
			),
		);
	}
}
