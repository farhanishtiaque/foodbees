import 'package:bubble/bubble.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:foodbees/globals.dart';
import 'package:foodbees/models/Chat.dart';
import 'package:foodbees/models/Message.dart';
import 'package:foodbees/models/User.dart';
import 'package:foodbees/services/FB.dart';
import 'package:foodbees/views/Donor/DonorLayout.dart';

// ignore: must_be_immutable
class ChatUi extends StatefulWidget {
  FB fb = FB();
  User user;
  Chat chat;
  PreferredSizeWidget appBar;

  ChatUi({@required Chat chat, User user}) {
    this.user = user;
    this.chat = chat;
    if (user.userType == 'Volunteer') {}
  }

  @override
  _ChatState createState() => _ChatState();
}

class _ChatState extends State<ChatUi> {
  List<CustomPopupMenu> choices = <CustomPopupMenu>[
    CustomPopupMenu(title: 'Profile', icon: Icons.person, val: 1),
    CustomPopupMenu(title: 'Sign Out', icon: Icons.exit_to_app, val: 2)
  ];
  List listMessage;

  @override
  Widget build(BuildContext context) {
    TextEditingController msg = TextEditingController();
    double pixelRatio = MediaQuery.of(context).devicePixelRatio;
    double px = 1 / pixelRatio;

    BubbleStyle styleSomebody = BubbleStyle(

      nip: BubbleNip.leftTop,
      color: Color.fromARGB(255, 231, 235, 237),
      elevation: 3 * px,
      margin: BubbleEdges.only(top: 5.0, right: 50.0),
      alignment: Alignment.topLeft,

    );
    BubbleStyle styleMe = BubbleStyle(

      nip: BubbleNip.rightTop,
      color: Color.fromARGB(255, 229, 41, 62),
      elevation: 3 * px,
      margin: BubbleEdges.only(top: 5.0, left: 50.0),
      alignment: Alignment.topRight,
    );
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          color: Colors.black,
          icon: Icon(Icons.keyboard_backspace),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: Text(
          widget.user.name,
          style: TextStyle(color: Colors.black),
        ),
        backgroundColor: Colors.white,
        actions: <Widget>[
          PopupMenuButton<CustomPopupMenu>(
            //elevation: 3.2,
            onSelected: (ch) {
              switch (ch.title) {
                case 'Profile':
                  break;
                case 'Sign Out':
                  widget.fb.auth.signOut().then((v) {
                    setState(() {
                      Navigator.pushNamed(context, '/');
                      loginUser = null;
                    });
                  });
                  break;
              }
            },
            padding: EdgeInsets.symmetric(vertical: 5, horizontal: 0),
            child: Image.network('https://ui-avatars.com/api/?length=2&bold=true&size=200&name=' + loginUser.name),
            itemBuilder: (BuildContext bc) {
              return choices.map((CustomPopupMenu choice) {
                return PopupMenuItem<CustomPopupMenu>(
                  value: choice,
                  child: Text(choice.title),
                );
              }).toList();
            },
          ),
        ],
      ),
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Expanded(
              child: StreamBuilder(
                stream: widget.fb.store
                    .collection(Collections.messages)
                    .where('chatID', isEqualTo: widget.chat.id)
                    .orderBy('time', descending: true)
                    .snapshots(),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    List listMessage = snapshot.data.documents;
                    return ListView.builder(
                        padding: EdgeInsets.all(10.0),
                        itemBuilder: (context, index) {
                          Message message = Message.fromJson((listMessage[index] as DocumentSnapshot).data);
                          return Bubble(
                            style: message.sender == loginUser.id ? styleMe : styleSomebody,
                            child: Text(message.body),
                          );
                        },
                        itemCount: snapshot.data.documents.length,
                        reverse: true);
                  } else {
                    return Center(child: CircularProgressIndicator());
                  }
                },
              ),
            ),
            Container(
              color: Colors.black12,

              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    child: TextField(
                      keyboardType: TextInputType.multiline,
                      maxLines: null,
                      decoration: InputDecoration(
                          contentPadding: EdgeInsets.symmetric(horizontal: 10),
                          hintText: "Type your message...",
                          focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide.none,
                          )),
                      controller: msg,
                    ),
                  ),
                  IconButton(
                    color: Color.fromARGB(255, 229, 41, 62),
                    icon: Icon(Icons.send),
                    onPressed: () {
                      if (msg.text.length > 0) {
                        Message message = Message(
                            body: msg.text,
                            chatID: widget.chat.id,
                            receiver: widget.user.id,
                            sender: loginUser.id,
                            time: DateTime.now());
                        widget.fb.store.collection(Collections.messages).add(message.toJson()).then((v) {
                          message.id = v.documentID;
                          v.updateData(message.toJson()).then((r) {
                            setState(() {
                              msg.text = '';
                            });
                          });
                        });
                      }
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
