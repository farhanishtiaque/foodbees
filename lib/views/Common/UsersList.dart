import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:foodbees/globals.dart';
import 'package:foodbees/models/Chat.dart';
import 'package:foodbees/models/User.dart';
import 'package:foodbees/services/FB.dart';
import 'package:foodbees/views/Common/ChatUi.dart';

// ignore: must_be_immutable
class UsersList extends StatefulWidget {
  FB fb = FB();

  @override
  _UsersListState createState() => _UsersListState();
}

class _UsersListState extends State<UsersList> {
  bool isCompleted = false;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: StreamBuilder(
        builder: (bc, snapshot) {
          if (snapshot.hasData) {
            List<User> userList = [];
            (snapshot.data as QuerySnapshot).documentChanges.forEach((f) {
              if (f.document.documentID != loginUser.id) {
                userList.add(User.fromJson(f.document.data));
              }
            });
            return ListView.builder(
              itemBuilder: (ic, index) {
                User user = userList[index];
                Icon trailing = Icon(Icons.trip_origin);
                dynamic chatUserIDList = loginUser.existChatUsers.map((f) => (f as dynamic).values.first);

                if (chatUserIDList.contains(user.id)) {
                  trailing = Icon(Icons.redo);
                }
                return Container(
                    margin: EdgeInsets.symmetric(vertical: 1, horizontal: 5),
                    color: Colors.white,
                    child: ListTile(
                        onTap: () {
                          Chat chat = Chat(startTime: DateTime.now(), user1: loginUser.id, user2: user.id);
                          if (!chatUserIDList.contains(user.id)) {
                            widget.fb.store.collection(Collections.chats).add(chat.toJson()).then((res) {
                              chat.id = res.documentID;
                              res.updateData(chat.toJson()).then((onValue) {
                                loginUser.existChatUsers.add({chat.id: user.id});
                                widget.fb.store
                                    .collection(Collections.users)
                                    .document(loginUser.id)
                                    .updateData(loginUser.toJson())
                                    .then((onLU) {
                                  user.existChatUsers.add({chat.id: loginUser.id});
                                  widget.fb.store
                                      .collection(Collections.users)
                                      .document(user.id)
                                      .updateData(user.toJson())
                                      .then((onUU) {
                                    setState(() {
                                      Navigator.push(
                                          context,
                                          new MaterialPageRoute(
                                            builder: (BuildContext context) => ChatUi(user: user, chat: chat),
                                          ));
                                    });
                                  });
                                });
                              });
                            });
                          } else {
                            Map<dynamic, dynamic> o = loginUser.existChatUsers.where((e) {
                              return (e as Map<dynamic, dynamic>).values.first == user.id;
                            }).first;
                            widget.fb.store.collection(Collections.chats).document(o.keys.first).get().then((v) {
                              Chat chat = Chat.fromJson(v.data);
                              Navigator.push(
                                  context,
                                  new MaterialPageRoute(
                                    builder: (BuildContext context) => ChatUi(user: user, chat: chat),
                                  ));
                            });
                          }
                        },
                        title: Text(user.name ?? ''),
                        subtitle: Text(user.userType ?? ''),
                        trailing: trailing,
                        leading: CircleAvatar(
                          radius: 20,
                          child: Image.network(
                              'https://ui-avatars.com/api/?length=2&bold=true&size=200&name=' + user.name),
                        )));
              },
              itemCount: userList.length,
            );
          } else {
            return CircularProgressIndicator();
          }
        },
        stream: widget.fb.store.collection(Collections.users).snapshots(),
      ),
    );
  }
}
