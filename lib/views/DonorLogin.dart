import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:foodbees/models/User.dart';
import 'package:foodbees/services/FB.dart';

import '../globals.dart';

// ignore: must_be_immutable
class DonorLogin extends StatefulWidget {
  FB fb = new FB();

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<DonorLogin> {
  @override
  Widget build(BuildContext context) {
    TextEditingController email = TextEditingController(text: 'brrinta@gmail.com');
    TextEditingController password = TextEditingController(text: 'qwerty@2021.');
    return Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomInset: false,
      body: Container(
        padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
        child: Column(
          //mainAxisAlignment: MainAxisAlignment.center,

          children: <Widget>[
            Spacer(),
            Container(
              alignment: Alignment.topLeft,
              padding: EdgeInsets.only(left: 15),
              child: Text(
                'Login',
                style: TextStyle(color: Color.fromARGB(255, 229, 41, 62), fontWeight: FontWeight.bold, fontSize: 33),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              alignment: Alignment.topLeft,
              padding: EdgeInsets.only(left: 15),
              child: Text(
                "Your Email",
                textAlign: TextAlign.left,
                style: TextStyle(
                  fontSize: 18,
                  color: Color.fromARGB(255, 229, 41, 62),
                ),
              ),
            ),
            SizedBox(
              height: 5,
            ),
            SizedBox(
              child: Container(
                height: 48,
                padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(30),
                  color: Color.fromARGB(255, 231, 235, 237),
                ),
                child: TextField(
                  //  controller: email,
                  decoration: InputDecoration(
                    hintText: "Your Email",
                    border: InputBorder.none,
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              alignment: Alignment.topLeft,
              padding: EdgeInsets.only(left: 15),
              child: Text(
                "Password",
                textAlign: TextAlign.left,
                style: TextStyle(
                  fontSize: 18,
                  color: Color.fromARGB(255, 229, 41, 62),
                ),
              ),
            ),
            Container(
              height: 48,
              padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(30),
                color: Color.fromARGB(255, 231, 235, 237),
              ),
              child: TextField(
                //controller: password,
                obscureText: true,
                decoration: InputDecoration(
                  hintText: "Your Password",
                  border: InputBorder.none,
                ),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              width: double.infinity,
              child: RaisedButton(
                  color: Color.fromARGB(255, 229, 41, 62),
                  padding: EdgeInsets.symmetric(vertical: 15),
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)),
                  child: Text(
                    "Login",
                    style: TextStyle(
                      fontSize: 18,
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  onPressed: () {
                    widget.fb.auth
                        .signInWithEmailAndPassword(email: email.text, password: password.text)
                        .then((AuthResult ar) {
                      widget.fb.auth.onAuthStateChanged.listen((user) {
                        if (user != null) {
                          widget.fb.store
                              .collection(Collections.users)
                              .where('email', isEqualTo: user.email)
                              .limit(1)
                              .getDocuments()
                              .then((users) {
                            Map<String, dynamic> usr = users.documents.first.data;
                            usr['id'] = users.documents.first.documentID;
                            loginUser = User.fromJson(usr);
                            Navigator.pushNamed(context, '/donorHome');
                          });
                        }
                      });
                    }).catchError((error) {
                      print(error);
                    });
                  }),
            ),
            FlatButton(
                child: Text(
                  "Can't Login? Forgot password",
                ),
                onPressed: () {}),
            Spacer(),
            Text("Don't have an accout?"),
            FlatButton(
                child: Text(
                  "Regsier",
                ),
                textColor: Color.fromARGB(255, 229, 41, 62),
                onPressed: () {
                  Navigator.pushNamed(context, '/donorregister');
                }),
            Spacer()
          ],
        ),
      ),
    );
  }
}
