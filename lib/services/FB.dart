import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

class FB {
	Firestore store = Firestore.instance;
	FirebaseAuth auth = FirebaseAuth.instance;
}


class Collections {
	static final String users = "users";
	static final String posts = "posts";
	static final String donations = "donations";
	static final String messages = "messages";
	static final String chats = "chats";
}