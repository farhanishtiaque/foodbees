import 'package:flutter/material.dart';
import 'package:foodbees/services/FB.dart';
import 'package:foodbees/views/Common/UsersList.dart';
import 'package:foodbees/views/Donor/DonorLayout.dart';
import 'package:foodbees/views/Donor/ui/DonationTimeLine.dart';
import 'package:foodbees/views/Donor/ui/DonorProfile.dart';
import 'package:foodbees/views/Donor/ui/donation_states.dart';
import 'package:foodbees/views/DonorLogin.dart';
import 'package:foodbees/views/DonorRegister.dart';
import 'package:foodbees/views/Volunteer/DonationDetails.dart';
import 'package:foodbees/views/Volunteer/VolunteerLayout.dart';
import 'package:foodbees/views/VolunteerLogin.dart';
import 'package:foodbees/views/VolunteerRegister.dart';
import 'package:foodbees/views/Welcome.dart';

import 'globals.dart';
import 'models/User.dart';
import 'views/Volunteer/VolunteerProfile.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  return runApp(App());
}

// ignore: must_be_immutable
class App extends StatefulWidget {
  FB fb = FB();

  @override
  AppState createState() => AppState();
}

class AppState extends State<App> {
  String initRoute = '/';
  Widget base = SplashScreen();
  Map<int, Color> color = {
    50: Color.fromRGBO(229, 41, 62, .1),
    100: Color.fromRGBO(229, 41, 62, .2),
    200: Color.fromRGBO(229, 41, 62, .3),
    300: Color.fromRGBO(229, 41, 62, .4),
    400: Color.fromRGBO(229, 41, 62, .5),
    500: Color.fromRGBO(229, 41, 62, .6),
    600: Color.fromRGBO(229, 41, 62, .7),
    700: Color.fromRGBO(229, 41, 62, .8),
    800: Color.fromRGBO(229, 41, 62, .9),
    900: Color.fromRGBO(229, 41, 62, 1),
  };

  @override
  void initState() {
    super.initState();
    widget.fb.auth.onAuthStateChanged.listen((user) {
      if (user != null) {
        widget.fb.store
            .collection(Collections.users)
            .where('email', isEqualTo: user.email)
            .limit(1)
            .getDocuments()
            .then((users) {
          Map<String, dynamic> usr = users.documents.first.data;
          usr['id'] = users.documents.first.documentID;
          loginUser = User.fromJson(usr);
        });
      } else {
        setState(() {
          base = Welcome();
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      builder: (context, child) =>
          MediaQuery(data: MediaQuery.of(context).copyWith(alwaysUse24HourFormat: false), child: child),
      debugShowCheckedModeBanner: false,
      title: 'FoodBees',
      theme: ThemeData(
        primarySwatch: MaterialColor(0xFFE5293E, color),
      ),
      initialRoute: '/',
      routes: {
        '/': (context) => base,
        '/donorregister': (context) => DonorRegister(),

        //donor
        '/donorLogin': (context) => DonorLogin(),
        '/donorHome': (context) => DonorLayout(),
        '/donationDetails': (context) => DonationDetailsVolunteer(),
        '/donation': (context) => DonationTimeLine(),
        '/donorprofile': (context) => DonorProfile(),
        '/donationstates': (context) => DonationState(),
        //
        '/volunteerHome': (context) => VolunteerLayout(),
        '/volunteerLogin': (context) => VolunteerLogin(),
        '/volunteerregister': (context) => VolunteerRegister(),
        '/volunteerProfile': (context) => VolunteerProfile(),

        //common
        '/chat': (context) => UsersList()
      },
    );
  }
}

class SplashScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return SplashScreenState();
  }
}

class SplashScreenState extends State<SplashScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'Welcome to FOODBees',
              style: TextStyle(fontSize: 50, fontWeight: FontWeight.bold),
              textAlign: TextAlign.center,
            ),
            CircularProgressIndicator(
              strokeWidth: 10,
              backgroundColor: Colors.white,
            )
          ],
        ),
      ),
    );
  }
}
