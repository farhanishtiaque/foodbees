import 'package:cloud_firestore/cloud_firestore.dart';

class Chat {
	DateTime startTime;
	String user1;
	String user2;
	DateTime lastTime;
	String id;

	Chat({ this.startTime, this.user1, this.user2, this.lastTime, this.id});

	Chat.fromJson(Map<String, dynamic> json) {
		startTime = json['startTime'] != null ? DateTime.fromMillisecondsSinceEpoch((json['startTime'] as Timestamp)
			.seconds *
			1000) : null;
		user1 = json['user1'];
		user1 = json['user2'];
		lastTime =
		json['lastTime'] != null ? DateTime.fromMillisecondsSinceEpoch((json['lastTime'] as Timestamp).seconds *
			1000) : null;
		id = json['id'];
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['startTime'] = this.startTime;
		data['user1'] = this.user1;
		data['user2'] = this.user2;
		data['lastTime'] = this.lastTime;
		data['id'] = this.id;
		return data;
	}
}