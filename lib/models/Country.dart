import 'dart:convert';

class Country {
	String id;
	String alpha3Code;
	String capital;
	List<String> timezones;
	String alpha2Code;
	List<String> callingCodes;
	String name;
	List<Currencies> currencies;

	Country({this.id, this.alpha3Code,
		this.capital,
		this.timezones,
		this.alpha2Code,
		this.callingCodes,
		this.name,
		this.currencies});

	_fromJson(Map<String, dynamic> json) {
		id = json['id'];
		alpha3Code = json['alpha3Code'];
		capital = json['capital'];
		timezones = json['timezones'].cast<String>();
		alpha2Code = json['alpha2Code'];
		callingCodes = json['callingCodes'].cast<String>();
		name = json['name'];
		if (json['currencies'] != null) {
			currencies = new List<Currencies>();
			json['currencies'].forEach((v) {
				//var o = jsonDecode(jsonEncode(v));
				currencies.add(Currencies.fromJson(v));
			});
		}
	}

	Country.cast(Map<String, dynamic> data, String id){
		var d = data;
		d['id'] = id;
		this._fromJson(jsonDecode(jsonEncode(d)));
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['alpha3Code'] = this.alpha3Code;
		data['capital'] = this.capital;
		data['timezones'] = this.timezones;
		data['alpha2Code'] = this.alpha2Code;
		data['callingCodes'] = this.callingCodes;
		data['name'] = this.name;
		data['id'] = this.id;
		if (this.currencies != null) {
			data['currencies'] = this.currencies.map((v) => v.toJson()).toList();
		}
		return data;
	}
}

class Currencies {
	String symbol;
	String code;
	String name;

	Currencies({this.symbol, this.code, this.name});

	Currencies.fromJson(Map<String, dynamic> json) {
		symbol = json['symbol'];
		code = json['code'];
		name = json['name'];
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['symbol'] = this.symbol;
		data['code'] = this.code;
		data['name'] = this.name;
		return data;
	}
}