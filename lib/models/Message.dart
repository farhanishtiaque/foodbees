import 'package:cloud_firestore/cloud_firestore.dart';

class Message {
	String body;
	DateTime time;
	String sender;
	String receiver;
	String chatID;
	String id;

	Message({ this.body, this.time, this.sender, this.receiver,
		this.chatID, this.id});

	Message.fromJson(Map<String, dynamic> json) {
		body = json['body'];
		time = DateTime.fromMillisecondsSinceEpoch((json['time'] as Timestamp).seconds * 1000);
		sender = json['sender'];
		receiver = json['receiver'];
		chatID = json['chatID'];
		id = json['id'];
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['body'] = this.body;
		data['time'] = this.time;
		data['sender'] = this.sender;
		data['receiver'] = this.receiver;
		data['chatID'] = this.chatID;
		data['id'] = this.id;
		return data;
	}
}