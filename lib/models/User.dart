

class User {
	String userType;
	String name;
	String institution;
	String occupation;
	String mobile;
	String email;
	String password;
	String id;
	List<dynamic> existChatUsers;

	User({ this.userType, this.name,this.institution, this.occupation, this.mobile,
		this .email, this .password, this.id, this.existChatUsers});

	User.fromJson(Map<String, dynamic> json) {
		userType = json['userType'];
		name = json['name'];
		institution =json['institution'];
		occupation = json['occupation'];
		mobile = json['mobile'];
		email = json['email'];
		password = json['password'];
		existChatUsers = json['existChatUsers'];
		id = json['id'];
	}

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['userType'] = this.userType;
		data['name'] = this.name;
		data['institution'] = this.institution;
		data['occupation'] = this.occupation;
		data['mobile'] = this.mobile;
		data['email'] = this.email;
		data['password'] = this.password;
		data['existChatUsers'] = this.existChatUsers;
		data['id'] = this.id;
		return data;
	}
}