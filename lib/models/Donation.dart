import 'package:cloud_firestore/cloud_firestore.dart';

enum donationStatus { POSTED, ACCEPTED,  PICKED, DELIVERED, DECLINED}

class Donation {
  String food;
  String location;
  DateTime pickUpDate;
  String pickUpTime;
  bool packagingNeed;
  String volunteer;
  DateTime postTime; //pending
  DateTime acceptedTime;
  DateTime declineTime;
  DateTime pickedTime;
  DateTime deliveryTime;
  int status;
  String id;
  String donorID;
  String amount;
  int partialStatus;

  Donation(
      {this.food,
      this.location,
      this.pickUpDate,
      this.pickUpTime,
      this.packagingNeed,
      this.volunteer,
      this.postTime,
      this.status,
      this.id,
      this.donorID,
      this.amount,
      this.acceptedTime,
      this.pickedTime,
      this.deliveryTime,
      this.partialStatus});

  Donation.fromJson(Map<String, dynamic> json) {
    food = json['food'];
    location = json['location'];
    pickUpDate = DateTime.fromMillisecondsSinceEpoch((json['pickUpDate'] as Timestamp).seconds * 1000);
    pickUpTime = json['pickUpTime'];
    packagingNeed = json['packagingNeed'];
    volunteer = json['volunteer'];
    postTime = DateTime.fromMillisecondsSinceEpoch((json['postTime'] as Timestamp).seconds * 1000);
    status = json['status'];
    donorID = json['donorID'];
    id = json['id'];
    amount = json['amount'];
    acceptedTime = json['postTime'] != null
        ? DateTime.fromMillisecondsSinceEpoch((json['postTime'] as Timestamp).seconds * 1000)
        : null;
    pickedTime = json['pickedTime'] != null
        ? DateTime.fromMillisecondsSinceEpoch((json['pickedTime'] as Timestamp).seconds * 1000)
        : null;
    deliveryTime = json['deliveryTime'] != null
        ? DateTime.fromMillisecondsSinceEpoch((json['deliveryTime'] as Timestamp).seconds * 1000)
        : null;
    partialStatus = json['partialStatus'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['food'] = this.food;
    data['location'] = this.location;
    data['pickUpTime'] = this.pickUpTime;
    data['pickUpDate'] = this.pickUpDate;
    data['packagingNeed'] = this.packagingNeed;
    data['volunteer'] = this.volunteer;
    data['postTime'] = this.postTime;
    data['status'] = this.status;
    data['donorID'] = this.donorID;
    data['id'] = this.id;
    data['amount'] = this.amount;
    data['acceptedTime'] = this.acceptedTime;
    data['pickedTime'] = this.pickedTime;
    data['deliveryTime'] = this.deliveryTime;
    data['partialStatus'] = this.partialStatus;
    return data;
  }
}
