import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:foodbees/services/FB.dart';

import '../globals.dart';
FB fb = FB();
class ProfileCover extends StatelessWidget {
  final String name;
  final Color color;
  final Color logoutIconColor;
  final String link;

  ProfileCover({this.name, this.link, this.color, this.logoutIconColor});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 190,
      decoration: BoxDecoration(

        color: color,
        borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(50), bottomRight: Radius.circular(50)),
      ),
      child: Column(

        children: <Widget>[
//          Row(
//
//            mainAxisAlignment: MainAxisAlignment.end,
//            children: <Widget>[
//              Container(
//
//                margin: EdgeInsets.fromLTRB(0, 40, 5, 0),
//                child: FlatButton(
//                  child: Column(
//                    children: <Widget>[
//                      Icon(
//                        Icons.power_settings_new,
//                        color: logoutIconColor,
//                        size: 40.0,
//                      ),
//                      Text(
//                        "Logout",
//                        style: TextStyle(
//                          color: logoutIconColor,
//                        ),
//                      )
//                    ],
//                  ),
//                  onPressed: () {
//                    print("log out button clicked");
//                  },
//                ),
//              ),
//            ],
//          ),
          SizedBox(
            height: 50,
          ),
          Column(

            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(

                  height: 100.0,
                  width: 100.0,
                  decoration: new BoxDecoration(
                    shape: BoxShape.circle,
                  ),

                  child: Hero(
                    tag: 'Profile',
                    child: ClipRRect(

                      borderRadius: BorderRadius.circular(75.0),
                      child: Image.network(
                          'https://ui-avatars.com/api/?length=2&bold=true&size=200&name=' + loginUser
                              .name),
                    ),
                  ))
            ],
          ),
//                Container(
//                  width: 100.0,
//                  height: 100.0,
//                  decoration: BoxDecoration(
//                    shape: BoxShape.circle,
//                    image: DecorationImage(
//                      fit: BoxFit.fill,
//                      image: NetworkImage(link),
//                    ),
//                  ),
//                ),
          SizedBox(
            height: 15,
          ),
        ],
      ),
    );
  }
}
