import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class RoundedSingleInfoTile extends StatelessWidget {
  final String title;
  final IconData iconData;
  RoundedSingleInfoTile({ this.title , this.iconData});
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 48,

      width: double.infinity,
      child: Padding(
        padding: const EdgeInsets.all(5.0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(

              child: Icon(
                iconData,
                color: Colors.black,
                size: 30.0,
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 5,
                vertical: 5,
              ),
              child: Text(
                title,
                style: TextStyle(
                  fontSize: 18,
                ),
              ),
            ),
          ],
        ),
      ),
      padding: EdgeInsets.only(left: 10),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(30),
        color: Color.fromARGB(255, 231, 235, 237),
      ),
    );
  }
}