import 'package:flutter/material.dart';

class RoundedInfoTile extends StatelessWidget {
  final String title;
  final String value;
  RoundedInfoTile({this.title,this.value});
  @override
  Widget build(BuildContext context) {
    return Container(
      // padding: EdgeInsets.only(left: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 20,
              vertical: 5,
            ),
            child: Text(title,
                style: TextStyle(
                  color: Colors.black87,
                  fontSize: 20,
                )),
          ),

          Container(
            height: 48,
            width: double.infinity,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 15,
                    vertical: 5,
                  ),
                  child: Text(
                    value,
                    style: TextStyle(

                      fontSize: 18,
                    ),
                  ),
                ),
              ],
            ),
            padding: EdgeInsets.only(left: 10),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(30),
              color: Colors.white,
            ),
          ),
        ],
      ),
    );
  }
}